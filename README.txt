https://github.com/nfroidure/gulp-iconfont


The intention with this repository is to create a font-awesome style font which incorporates all commonly used vectors across the NWX brands, Gallo and Revo. This includes logos and wireframe icons.

The intention is that it can be simply added to each site in a few lines of code rather than have to import all assets individually. This also means that updates can be rolled out in a far more uniform manner